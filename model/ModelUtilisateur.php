<?php
class ModelUtilisateur
{

    private $dao;

    public function __construct()
    {
        $this->dao = new DAO;
    }

    public function getRoles()
    {
        return $this->dao->requete("
            SELECT * FROM role;
        ");
    }

    public function getCentres()
    {
        return $this->dao->requete("
        SELECT c.idCentre, c.nom FROM centre as c;
    ");
    }
}
