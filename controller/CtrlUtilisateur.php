<?php
class CtrlUtilisateur
{
    private $view;
    private $model;

    public function __construct(){
        $this->view = new ViewUtilisateur;
        $this->model = new ModelUtilisateur;
    }

    public function getFormUtilisateur(){
        $dataRoles = $this->model->getRoles();
        $dataCentres = $this->model->getCentres();
        $this->view->afficherFormUtilisateur($dataRoles, $dataCentres);
    }
}