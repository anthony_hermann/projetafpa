<?php

function __autoload($class_name)
{
    $modules = array(
        "",
        "Modules/Contact/",
        "Modules/Page/",
        "Modules/User/",
        "Modules/Meteo/"
    );

    $packages = array(
        "",
        "controller",
        "model",
        "view",
        "dao",
        "config",
        "system"
    );
    $pastrouve = true;
    
    $m = 0;
    while($pastrouve && $m < count($modules)){
        $i = 0;
        while($pastrouve && $i < count($packages)){
            // echo $modules[$m] . $packages[$i] . "/" . $class_name . '.php <br/>';
            if (file_exists($modules[$m] . $packages[$i] . "/" . $class_name . '.php')) {
                include_once $modules[$m] . $packages[$i] . "/" . $class_name . '.php';
                $pastrouve = false;
            }
            $i++;
        }
        // echo "<br/>";
        $m++;
    }

    if($pastrouve) {
       echo "Erreur de file<br/>";
    }

}
