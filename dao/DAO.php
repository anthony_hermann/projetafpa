<?php
class DAO
{
    private $mysqli;

    public function __construct()
    {
        $this->connexion();
    }

    public function requete($sql)
    {
        // die($sql);
        $this->mysqli->set_charset("utf8");
        if (!$result = $this->mysqli->query($sql)) {
            // Oh non ! La requête a échoué. 
            echo "Désolé, le site web subit des problèmes.";

            // Denouveau, ne faite pas ceci sur un site public, mais nous vous
            // montrerons comment récupérer les informations de l'erreur
            echo "Error: Notre requête a échoué lors de l'exécution et voici pourquoi :\n";
            echo "Query: " . $sql . "\n";
            echo "Errno: " . $this->mysqli->errno . "\n";
            echo "Error: " . $this->mysqli->error . "\n";
            exit;
        } else {
            $data = [];
            if (is_object($result)) {
                while ($row = $result->fetch_assoc()) {
                    $data[] = $row;
                }
                return $data;
            }
        }
        return true;
    }


    private function connexion()
    {
        $this->mysqli = new mysqli(
            'localhost',
            'philippe',
            'philippe31',
            'projetafpa'
        );

        // Oh non ! Une connect_errno existe donc la tentative de connexion a échoué !
        if ($this->mysqli->connect_errno) {
            // La connexion a échoué. Que voulez-vous faire ? 
            // Vous pourriez vous contacter (email ?), enregistrer l'erreur, afficher une jolie page, etc.
            // Vous ne voulez pas révéler des informations sensibles

            // Essayons ceci :
            echo "Désolé, le site web subit des problèmes.";

            // Quelque chose que vous ne devriez pas faire sur un site public,
            // mais cette exemple vous montrera quand même comment afficher des
            // informations lié à l'erreur MySQL -- vous voulez peut être enregistrer ceci
            echo "Error: Échec d'établir une connexion MySQL, voici pourquoi : \n";
            echo "Errno: " . $this->mysqli->connect_errno . "\n";
            echo "Error: " . $this->mysqli->connect_error . "\n";

            // Vous voulez peut être leurs afficher quelque chose de jolie, nous ferons simplement un exit
            exit;
        }
    }
}
